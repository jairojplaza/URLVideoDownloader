package com.example.theplazamen.uvdownloader;

import android.graphics.Bitmap;

/*
 *   Esquema de datos para la lista
 *   INPUT NULL
 *   OUTPUT String  nombreVideo
 *   OUTPUT Bitmap  ThumbnailVideo
 */
public class SearchResults {
    private String name = "";
    private Bitmap bitmap;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setBitmap(Bitmap bitmap){
        this.bitmap = bitmap;
    }

    public Bitmap getBitmap(){
        return bitmap;
    }

}//Fin class