package com.example.theplazamen.uvdownloader;

import android.content.Context;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;
import static com.example.theplazamen.uvdownloader.MainActivity.ruta;

public class SearchVideoFromUrl {

	/*
	 * Expresiones regulares para conseguir las extensiones
	 * "/^(.*.(?!(mp4|mkv|wmv)$))?[^.]*$/i"
	 * "^.*.(avi|AVI|wmv|WMV|flv|FLV|mpg|MPG|mp4|MP4)$"
	 */

	private String[] formatos;		//Array String para guardar los formatos de video que soporta nuestra aplicación
	private String url;				//String de la url que se necesita para que funcione esta clase

	private static Context context;	//Contexto, usado para poder enviar mensajes Toast desde una clase que no extiende del main
    private ToastMessage toast;
    private Downloader downloader;
	//Constructor de la clase
	public SearchVideoFromUrl(String url, Context context) {

		this.url = url; // "https://www.w3schools.com/html/html5_video.asp"
		this.context    = context;
		this.toast      = new ToastMessage(context);
		downloader		= new Downloader(context);
		formatos = new String[6];
		formatos[0] = "mp4";
		formatos[1] = "flv";
		formatos[2] = "mkv";
		formatos[3] = "wmv";
		formatos[4] = "ogg";
		formatos[5] = "mpg";
	}//Fin constructor


	/*
	 *	Bucle encargado de ejecutar el metodo de descarga las veces necesarias para
	 *	descargar los videos de la página web en cuestión
     *  INPUT   NULL
     *  OUTPUT  NULL
     */
	public String [] datosVideo( String  url ){

		String nombre = url.substring(url.lastIndexOf("/"), url.lastIndexOf("."));


		String [] datos = new String[2];
		datos[0] = url;
		datos[1] = nombre;


		return datos;

	}//Fin datosVideo


	/*
	 *	Metodo para obtener la extensión de una url
	 *	INPUT 	String url
	 *	OUTPUT	String ext
	 */
	public String ext( String  url ){
		String ext = url.substring(url.lastIndexOf(".")+1, url.length());
		toast.mensaje("La ext "+ext);
		return ext;
	}//Fin ext


	/*
	 *	Metodo para comprobar si la url pertenece a un video
	 * 	INPUT	null
	 * 	OUTPUT	boolean	true/false
	 */
    public boolean isVideoUrl(){
        String  ext = ext(url);
        for (int i = 0; i < formatos.length; i++) {
            String formato = formatos[i];
            if (ext.equalsIgnoreCase(formato)) {

                return true;
            }
        }
	    return false;
    }//Fin isVideoUrl


    /*
     *	Metodo encargado de obtener las urls de los videos indexados, los cuales acaben en .ext
     *	Los mete en un array y los devuelve para su posterior tratamiento
     *  INPUT   String url
     *  OUTPUT  CharSequence[] url_map
     */
	public CharSequence[] getVideoSRC() {
		try {
			InputStream is = getVideoPage(url);
			String videoUrl = null;
			String video = "";
			if (is == null) {
				return null;
			}
			LinkedList<String> urls = new LinkedList<String>();

			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "UTF-8"));

			//Tratamiento por linea
			while (reader.readLine() != null) {

				videoUrl = null;
				String linea = reader.readLine();
				if (linea != null && !linea.isEmpty()) {
                    //Busqueda de la etiqueta video discriminando por esta y por source
					if (linea.contains("video") && linea.contains("src")) {
						for (int i = 0; i < formatos.length; i++) {
							String formato = formatos[i];
							if (linea.contains(formato)) {
								video = linea.substring(
										linea.indexOf("src=\"") + 5,
										linea.indexOf(formato) + 3);
								if (video.contains(";")) {
									video = video.replace(video.substring(
											video.indexOf(";") - 1,
											video.lastIndexOf(";") + 1), "");
								}

								videoUrl = url.substring(0,
										url.lastIndexOf("/") + 1);
								videoUrl += video;
								urls.add(videoUrl);
							}
						}
					}
				}
			}
			CharSequence[] url_map = urls.toArray(new String[urls.size()]);

			return url_map;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}// Fin getUrls


    /*
     *  Metodo que establece una conexión y devuelve un InputStream correspondiente a la página
     *  INPUT   String url
     *  OUTPUT  InputStream connection.getInputStream()
     */
	public InputStream getVideoPage(String url) {
		try {

			HttpURLConnection connection = downloader.con(url);
			connection.setRequestMethod("GET");
			return connection.getInputStream();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}//Fin getVideoPage

}//Fin class
