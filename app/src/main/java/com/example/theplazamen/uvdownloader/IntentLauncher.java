package com.example.theplazamen.uvdownloader;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;

import java.io.File;

public class IntentLauncher {

    Intent intent;
    Activity activity;

    public IntentLauncher(Activity activity){

        this.activity = activity;

    }


    /*
     *  Metodo para lanzar un intent
     *  Si se necesita compartir algún dato con esa clase
     *  se envia el dato y el valor de éste en caso contrario son null
     *  INPUT   Class   claseDestino
     *  INPUT   String  nombreDatoAenviar
     *  INPUT   String  ValorDatoAenviar
     */
    public void lanzarIntent(Class clase, String nombre, String valor){

        intent = new Intent(activity,clase);

        if((nombre!=null && !nombre.isEmpty()) || (valor!=null && !valor.isEmpty())) {

            intent.putExtra(nombre, valor);
        }

        activity.startActivity(intent);

    }//Fin lanzarIntent


    /*
     *  Metodo para reproducir el video con un reproductor externo
     *  INPUT Uri uriDelVideo
     */
    public void reproductorExterno(Uri uri){

        intent = new Intent(Intent.ACTION_VIEW);

        intent.setDataAndType(uri, "video/*");

        activity.startActivity(intent);

    }//Fin reproductorExterno


    /*
     *  Metodo para compartir el video en cuestión
     *  INPUT   String ruta
     */
    public void compartirArchivo(String miRuta){
        File videoEnCuestion;

        videoEnCuestion = new File(miRuta);
        intent          = new Intent(Intent.ACTION_SEND);

        if(videoEnCuestion.exists()) {

            intent.setType("application/video");
            intent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://"+miRuta));

            intent.putExtra(Intent.EXTRA_SUBJECT,
                    "Sharing File...");
            intent.putExtra(Intent.EXTRA_TEXT, "Envio un video y pasa ésto...");

            activity.startActivity(Intent.createChooser(intent, "Envio un video y pasa ésto..."));
        }

    }//Fin compartirArchivo


    /*
     *  Metodo para enviar un mensaje por medio de las aplicaciones de mensajería
     *  INPUT   String mensaje
     *  INPUT   String asunto
     */
    public void enviarMensaje(String mensaje, String asunto){

        String shareBody        = mensaje;
        Intent sharingIntent    = new Intent(android.content.Intent.ACTION_SEND);

        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, asunto);
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);

        activity.startActivity(Intent.createChooser(sharingIntent, "Titulo aquí"));

    }//Fin enviarMensaje

}//Fin class
