package com.example.theplazamen.uvdownloader;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import static android.content.Context.CLIPBOARD_SERVICE;

public class MainActivityDownload extends Fragment {

    private AlertDialog.Builder builder;                //  Constructor del alerDialog de las opciones del boton flotante
    private CharSequence opciones[];                    //  Opciones del menú contextual
    private EditText etUrl;                             //  EditText encargado de recoger la url, dentro del AlertDialog de descarga
    private Button btDescargar;                         //  Boton de descarga dentro del AlerDialog de descarga
    private Button btVerVideo;                          //  Boton para ver videos desde url
    private SearchVideoFromUrl searchVideoFromUrl;      //  Objeto del metodo que busca y descarga el video
    private HistorialBD historialBD;                    //  Objeto de base de datos
    private ClipboardManager myClipboard;               //  Manager del portapapeles
    private ClipData myClip;                            //  Dato del portapapeles
    private String texto;                               //  Texto del clip
    private ToastMessage toast;                         //  Clase para generar mensajes toast
    private IntentLauncher intent;                      //  Clase para lanzar intent
    private Downloader downloader;                      //  Objeto de la clase Downloader
    private Context context;                            //  Variable para guardar el contexto
    private Activity activity;                          //  Variable para guardar ésta actividad

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.activity_main_download, null);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        //Inicialización de variables
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        context     = getContext();
        activity    = getActivity();
        etUrl       = view.findViewById(R.id.url);
        btDescargar = view.findViewById(R.id.btBuscarVideos);
        btVerVideo  = view.findViewById(R.id.btVerVideo);
        historialBD = new HistorialBD(activity);
        toast       = new ToastMessage(context);
        intent      = new IntentLauncher(activity);
        downloader  = new Downloader(context);


        //Copia directamente desde el Portapapeles la url copiada por el usuario
        copiarDesdeClipBoard();


        /*
         *  Boton para reproducir video sin descargarlo
         */
        btVerVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!etUrl.getText().toString().isEmpty()) {
                    final String laUrl = etUrl.getText().toString();
                    searchVideoFromUrl = new SearchVideoFromUrl(laUrl, activity);
                    //toast.mensaje(laUrl.substring(laUrl.length()-4,laUrl.length()-3));
                    if(laUrl.contains("http") && ".".equalsIgnoreCase(laUrl.substring(laUrl.length()-4,laUrl.length()-3))) {

                        if(searchVideoFromUrl.isVideoUrl()) {

                            intent.lanzarIntent(MainActivityPlayer.class, "url_video", laUrl);

                        }else{

                            toast.mensaje("Formato de url no soportado");

                        }

                    }else{

                        toast.mensaje("Formato de url no soportado");

                    }
                }
            }
        });


        /*
         *    Boton de descargar
         *    Metodo que se comprueba si la url está vacía
         *    llama al metodo de descarga en caso contrario
         */
        btDescargar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!etUrl.getText().toString().isEmpty()) {

                    final String laUrl  = etUrl.getText().toString();
                    searchVideoFromUrl  = new SearchVideoFromUrl(laUrl, activity);

                    if (laUrl.contains("http") && ".".equalsIgnoreCase(laUrl.substring(laUrl.length()-4,laUrl.length()-3))&&searchVideoFromUrl.isVideoUrl()) {

                        CharSequence[] video;

                        video   = new CharSequence[]{laUrl};
                        builder = new AlertDialog.Builder(activity);
                        builder.setTitle("UVDownloader");
                        builder.setItems(video, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int pos) {
                                String[] datos;

                                datos = searchVideoFromUrl.datosVideo(laUrl);
                                String u = datos[0]; //  URL del video
                                String n = datos[1]; //  Nombre del video
                                //boolean hecho = searchVideoFromUrl.descargarVideo(u, n, e);

                                if (downloader.download(u,n)) {

                                    historialBD.insert(u);
                                    toast.mensaje("Descargado correctamente");

                                } else {

                                    toast.mensaje("Error inesperado");

                                }

                            }
                        });

                        builder.show();

                    }else{

                        opciones = searchVideoFromUrl.getVideoSRC();

                        if (opciones != null && opciones.length > 0) {

                            builder = new AlertDialog.Builder(activity);
                            builder.setTitle("Elije una opción");
                            builder.setItems(opciones, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int pos) {
                                    String[] datos;

                                    datos = searchVideoFromUrl.datosVideo(opciones[pos].toString());
                                    String u = datos[0]; //  URL del video
                                    String n = datos[1]; //  Nombre del video
                                    if (downloader.download(u, n)) {

                                        historialBD.insert(u);
                                        toast.mensaje("Descargado correctamente");

                                    } else {

                                        toast.mensaje("Error inesperado");

                                    }

                                }
                            });

                            builder.show();

                        } else {    //Fin if datos ok

                            CharSequence[] error;
                            error = new CharSequence[]{"No se han podido encontrar videos"};
                            builder = new AlertDialog.Builder(activity);
                            builder.setTitle("Ups!!");
                            builder.setItems(error, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });

                            builder.show();
                        }
                    }
                } else {    // Fin if empty

                    toast.mensaje("URL vacia");
                }
            }
        });
    }//Fin onViewCreated


    /*
     *  Metodo onActivityCreated guarda la instancia
     */
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

    }//Fin onActivityCreated


    /*
     *  Metodo que comprueba el portapapeles para saber si
     *  contiene una url
     *  INPUT   null
     *  OUTPUT  boolean isTrue
     */
    public boolean isClipboardTextUrl(){

        try {
            myClipboard = (ClipboardManager) activity.getSystemService(CLIPBOARD_SERVICE);
            myClip      = myClipboard.getPrimaryClip();
            texto       = myClip.getItemAt(0).coerceToText(activity).toString();

            if (etUrl != null && !etUrl.getText().toString().isEmpty()) {

                etUrl.setText("");

            }
            if (texto != null && !texto.isEmpty() && texto.contains("http")) {
                return true;
            }
        }catch (NullPointerException e){

        }
        return false;

    }//Fin isClipboardTextUrl


    /*
     *  Si el texto del portapapeles es una url
     *  Lo copia al EditText de la url
     */
    public void copiarDesdeClipBoard(){
        if(isClipboardTextUrl()){

            etUrl.setText(texto);

        }
    }//Fin copiarDesdeClipBoard


    /*
     *  Metodo para refrescar un fragment
     */
    public void refrescarFragment(){
        try {

            getFragmentManager().beginTransaction().detach(this).attach(this).commit();

        }catch (Exception e){

        }
    }//Fin refrescarFragment

}
