package com.example.theplazamen.uvdownloader;

import android.Manifest;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.View;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.content.Intent;

public class MainActivity extends AppCompatActivity{


    //Declaracioón de miembros dato privados

    private Intent intent;                              //  Intent encargado de la acción
    private View miView;                                //  View generico para usar en cualquier metodo
    private static ProgressDialog mProgressDialog;      //  Dialogo de carga al inicio de la aplicación
    private static Runnable cargandoRunnable;           //  Runnable para detener el dialogo de carga
    private ToastMessage toastMessage;                  //  Clase para mensajes toast
    MainActivityVideos fragmentVideos;                  //  ActivityFragment de la lista de los videos
    MainActivityHistorical fragmentHistorial;            //  ActivityFragment del historial de descargas
    MainActivityDownload fragmentDownload;              //  ActivityFragment para descargar


    //Miembros dato estáticos
    static final String nombreCarpeta = "UVDownlodader";//  Nombre de la carpeta de la aplicación
    static final String ThumbsCarpeta = "Thumbnails";   //  Nombre de la carpeta de Thumbnails
    static final String ruta = Environment.getExternalStorageDirectory().toString()+ "/"+nombreCarpeta;//   Ruta de la carpeta
    private final Context context = MainActivity.this;

    /*
    *   Metodo onCreate, encargado de la inicialización de los objetos cuando se abre la aplicación
    */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Comprobación de que la aplicación tiene permisos de escritura
        if (Build.VERSION.SDK_INT >= 23) {
            isStoragePermissionGranted();
                //System.exit(0);
        }

        //Mensaje de espera al inicio de la aplicación
        cargandoVideos(context);

        //Inicialización de variables y enlace de objetos
        toastMessage = new ToastMessage(context);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        fragmentVideos = new MainActivityVideos();
        fragmentHistorial = new MainActivityHistorical();
        fragmentDownload = new MainActivityDownload();


        //Define por defecto en el Fragment el Activity a mostrar al iniciar la aplicación
        lanzarFragment(fragmentVideos, "Mis videos");

    }//Fin onCreate


    /*
     *  Metodo encargado del control del menú
     */
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.itemVideos:       //  Manda al usuario a la librería de videos
                    lanzarFragment(fragmentVideos,"Mis videos");
                    return true;
                case R.id.itemHistorial:    //  Manda al usuario al historial de descargas
                    lanzarFragment(fragmentHistorial, "Historial de descargas");
                    return true;
                case R.id.itemDescargar:    //Manda al usuario al formulario de descarga
                    lanzarFragment(fragmentDownload, "Buscar videos");
                    fragmentDownload.refrescarFragment();
                    return true;
            }
            return false;
        }
    };//Fin mOnNavigationItemSelectedListener


    /*
     *  Metodo para lanzar un ActivityFragment
     *  INPUT   Fragment fragmento --> android.support.v4.app.Fragment;
     *  INPUT   String titulo
     */
    public void lanzarFragment(Fragment fragmento, String titulo){

        setTitle(titulo);
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        getSupportFragmentManager().beginTransaction().replace(R.id.container, fragmento,
                fragmento.getClass().getSimpleName()).addToBackStack(null).commit();
        fragmentTransaction.commit();
    }//Fin lanzarFragment


    /*
     *  Metodo para mostrar un dialogo de progreso al iniciar la vista de la librería de videos
     *  INPUT   Context contexto
     */
    protected static void cargandoVideos(Context context) {
        if (mProgressDialog == null) {

            mProgressDialog = ProgressDialog.show(context, "Cargando videos...", "Por favor espere...", true, true);
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.setMax(5);
            mProgressDialog.setOnCancelListener(new ProgressDialog.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                }
            });

        } else {
            mProgressDialog.show();
        }

         cargandoRunnable = new Runnable() {

            @Override
            public void run() {
                mProgressDialog.cancel();
            }
        };

        Handler pdCanceller = new Handler();
        pdCanceller.postDelayed(cargandoRunnable, 2500);

        //dismissWaitDialog();
    }//Fin cargandoVideos


    /*
     *  Metodo para quitar el mensaje de carga de la aplicación
     *  Todavía en testing
     */
    private void quitarCargandoVideos() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }//Fin quitarCargandoVideos


    /*
     *  Metodo para comprobar si el usuario ha otorgado los permisos de almacenamiento
     */
    @RequiresApi(api = Build.VERSION_CODES.M)
    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            return true;
        }
    }//Fin isStoragePermissionGranted


    /*
     *  Metodo para pedir permisos de almacenamiento al usuario
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults[0]== PackageManager.PERMISSION_GRANTED){

        }
    }//Fin onRequestPermissionsResult

}//Fin class
