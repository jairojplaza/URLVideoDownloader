package com.example.theplazamen.uvdownloader;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;


public class MainActivityPlayer extends AppCompatActivity {

    private VideoView videoView;                //  VideoView encargado de reproducir el video
    private String videoUrl;                    //  URL del video
    private Uri uri;                            //  uri para enviar al videoView
    private Context context;                    //  Contexto
    private ToastMessage toast;                 //  Metodo para mensajes toast
    private SearchVideoFromUrl searchVideo;     //  Objeto de la clase SearchVideoFromUrl
    private MediaController mediaController;    //  Para el control de reproducción

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_player);

        //Inicialización de variables
        context         = MainActivityPlayer.this;
        videoView       = (VideoView) findViewById(R.id.videoView);
        videoUrl        = getIntent().getStringExtra("url_video");
        toast           = new ToastMessage(context);
        searchVideo     = new SearchVideoFromUrl(videoUrl,context);
        mediaController = new  MediaController(context);
        uri             = Uri.parse(videoUrl);

        //Inicialización de videoView
        videoView.setVideoURI(uri);

        //Control de reproducción del video
        mediaController.setAnchorView(videoView);
        videoView.setMediaController(mediaController);

        //Acción cuandoa caba el video
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {

                mp.stop();
                finish();
            }
        });

        videoView.start();  //  Inicialización del videoView

    }//Fin onCreate

}//Fin class
