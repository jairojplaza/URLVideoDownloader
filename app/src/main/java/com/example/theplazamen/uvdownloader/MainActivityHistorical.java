package com.example.theplazamen.uvdownloader;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.hardware.SensorManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import static android.content.Context.CLIPBOARD_SERVICE;

public class MainActivityHistorical extends Fragment {

    private ListView listaHistorial;                            //  Lista de los enlaces de descarga efectuados correctamente
    private ArrayList<String> urlsHistorial;                    //  Url's de los videos descargados
    private ArrayAdapter<String> adaptador;                     //  Adaptador de la lista de nombres
    private String objetoEnCuestion;                            //  Objeto referenciado
    private HistorialBD historialBD;                            //  Objeto del la base de datos
    private Context context;                                    //  Contexto del Activity
    private ArrayList<String> urls;                             //  Nombres de los videos dentro de la carpeta de la aplicación
    private Object[] historial;                                 //  Objeto que contiene las url's y las id's
    private ClipboardManager myClipboard;                       //  Manager para copiar al portapapeles
    private ClipData myClip;                                    //  Para copiar al portapapeles
    private ArrayList<String> ids;                              //  Lista de las id's de las url's
    private SensorManager sensorManager;                        //  Para obtener el contexto
    private ToastMessage toastMessage;                          //  Clase para mensajes toast

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.activity_main_historial, null);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        context         = getActivity();
        historialBD     = new HistorialBD(context);
        toastMessage    = new ToastMessage(getActivity());
        urlsHistorial   = historialBD.selectEmAll();
        listaHistorial  = (ListView) view.findViewById(R.id.lvHistorial);
        adaptador       = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, urlsHistorial);
        sensorManager   = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);

        //Insertar adaptador a listView
        listaHistorial.setAdapter(adaptador);

        //insertarUrl("https://www.w3schools.com/html/html5_video.asp");

        //Listar URL's
        listarUrls();


        /*
         *   Metodo que determina la accion que se llevará a cabo a la hora de hacer click
         *   en un item de la lista
         */
        listaHistorial.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long id) {

                objetoEnCuestion =  listaHistorial.getItemAtPosition(position).toString();

                toastMessage.mensaje(objetoEnCuestion);
            }

        });


        /*
         *   Metodo que determina la accion que se llevará a cabo a la hora de hacer click
         *   prolongado en un item de la lista
         */
        listaHistorial.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if(historialBD.delete(ids.get(position).toString())){
                    toastMessage.mensaje("Eliminado correctamente");
                    refrescarFragment();
                }
                return false;
            }
        });



        listaHistorial.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if(copiarUrlAlPortapapeles(urls.get(position).toString())){
                    toastMessage.mensaje("Url copiada");
                }else{
                    toastMessage.mensaje("Error al copiar la url");
                }
            }
        });

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);



    }


    /*
     *  Metodo para copiar al portapapeles
     */
    public boolean copiarUrlAlPortapapeles(String url){

        try {
            myClipboard = (ClipboardManager) getActivity().getSystemService(CLIPBOARD_SERVICE);

            myClip = ClipData.newPlainText("text", url);
            myClipboard.setPrimaryClip(myClip);
            if (myClip != null && !myClip.equals("")) {
                return true;
            }
        }catch (NullPointerException e){}
        return false;
    }


    /*
     *   Lista las url's dentro del historial
     */
    public void listarUrls(){

        historial = historialBD.selectAll();
        if(historial!=null) {

            ids = (ArrayList<String>) historial[0];
            urls = (ArrayList<String>) historial[1];
        }
    }


    /*
     *  Inserta una url en el historial
     *  INPUT   String url
     *  OUTPUT  boolean false
     */
    public boolean insertarUrl(String url){
        historialBD.insert(url);
        getActivity().finish();
        startActivity(getActivity().getIntent());
        return false;
    }


    /*
     *  Metodo para refrescar éste fragmento
     */
    public void refrescarFragment(){
        try {
            getFragmentManager().beginTransaction().detach(this).attach(this).commit();
        }catch (Exception e){}
    }
}
