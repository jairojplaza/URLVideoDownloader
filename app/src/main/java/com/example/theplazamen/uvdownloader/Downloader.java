package com.example.theplazamen.uvdownloader;

import android.content.Context;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import static com.example.theplazamen.uvdownloader.MainActivity.ruta;

public class Downloader {

    private ToastMessage toast;         //  Para enviar mensajes toast
    private String videoUrl;            //  URL del video
    private String nombre;              //  Nombre del video


    public Downloader(Context context){
        toast = new ToastMessage(context);
    }



    /*
     *	Metodo que recibe una url y establece una conexión con esta aparentando ser un navegador
     *  INPUT   String url
     *  OUTPUT  HttpURLConnection connection
     */
    public HttpURLConnection con(String url) throws Exception {
        URL u = new URL(url);
        //mensajeToast("Mensaje desde HttpURLCONECCTION");
        HttpURLConnection connection = (HttpURLConnection) u.openConnection();
        connection
                .setRequestProperty(
                        "User-Agent",
                        "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 " +
                                "(KHTML, like Gecko) Chrome/27.0.1453.110 Safari/537.3");
        return connection;
    }//Fin con


    /*
     *  Metodo para descargar el video
     *  INPUT   String url
     *  OUTPUT  Nada
     */
    public boolean download( String videoUrl, String nombre){

        toast.mensaje("Intentando descargar " + videoUrl + "...");
        boolean correcto = false;
        try {
            HttpURLConnection con = con(videoUrl);

            String type = con.getContentType();
            type = con.getContentType().substring(type.lastIndexOf("/")+1);    //Para obtener la extensión

            String namae = ruta+nombre+"."+type;
            InputStream is = con.getInputStream();
            FileOutputStream fos = new FileOutputStream(new File(namae));
            byte[] buffer = new byte[4096];
            int bytesRead;
            int length = con.getContentLength();
            int read = 0;
            while ((bytesRead = is.read(buffer, 0, 4096)) != -1) {
                read += bytesRead;
                //  toast.mensaje(read + " out of " + length);
                fos.write(buffer, 0, bytesRead);
            }
            if (fos != null) {
                //  toast.mensaje("Archivo <<" + namae + ">> descargado correctamente");
                correcto = true;
            }
            fos.flush();
            fos.close();


        } catch (MalformedURLException e) {

            toast.mensaje(e.getMessage().toString());

        } catch (FileNotFoundException e){

            toast.mensaje(e.getMessage().toString());

        } catch (IOException e){

            toast.mensaje(e.getMessage().toString());

        } catch (Exception e){

            toast.mensaje(e.getMessage().toString());

        }
        return correcto;
    }//Fin descargarVideo
}