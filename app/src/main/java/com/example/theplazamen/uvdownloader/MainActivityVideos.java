package com.example.theplazamen.uvdownloader;

import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class MainActivityVideos extends Fragment {


    private ListView listaVideos;                               //  Lista de los videos
    private ArrayList<String> nombres;                          //  Nombres de los videos dentro de la carpeta de la aplicación
    private ArrayAdapter<Bitmap> adaptador;                     //  Adaptador de la lista de nombres
    private String objetoEnCuestion;                            //  Hace referencia a un objeto en concreto (dentro de la lista)
    private String ruta = MainActivity.ruta;                    //  Carpeta de la aplicación definida en MainActivity
    private String nombreCarpeta = MainActivity.nombreCarpeta;  //  Nombre de la carpeta definida en MainActivity
    private String ThumbsCarpeta = MainActivity.ThumbsCarpeta;  //  Nombre de la carpeta definida en MainActivity
    private ArrayList<Bitmap> thumbs;                           //  Array de Thumbnails(Miniaturas) de los videos
    private ToastMessage toast;                                 //  Clase para mandar mensajes toast
    private AlertDialog.Builder builder;                        //  Constructor de AlertDialog
    private IntentLauncher intent;                              //  Clase para lanzar un intent para distintos fines


    /*
     *  El metodo onCreate se encarga de la vista del Fragment al ser invocado
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.activity_main_videos, null);
        return root;
    }


    /*
     *  Una vez se ha cargado la vista se inicializan todas las variables y metodos a usar en el ActivityFragment
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        MainActivity.cargandoVideos(getContext());
        thumbs      = new ArrayList<Bitmap>();
        nombres     = new ArrayList<String>();
        listaVideos = (ListView) view.findViewById(R.id.listaVideos);
        adaptador   = new ArrayAdapter<Bitmap>(getActivity(),android.R.layout.simple_list_item_1,thumbs);
        toast       = new ToastMessage(getActivity());
        intent      = new IntentLauncher(getActivity());

        ArrayList<SearchResults> searchResults = getSearchResults();

        //Lista los videos dentro de la aplicación, hace una comprobación previa de que la carpeta está creada
        listaVideos.setAdapter(new CustomAdapter(getActivity(), searchResults));


        /*
         *   Metodo que determina la accion que se llevará a cabo a la hora de hacer click
         *   en un item de la lista
         */
        listaVideos.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long id) {

                objetoEnCuestion    =  nombres.get(position).toString();
                String miRuta       = ruta + "/"+objetoEnCuestion;

                intent.lanzarIntent(MainActivityPlayer.class, "url_video", miRuta);

            }

        });//Fin listaVideos.setOnItemClickListener


        /*
         *   Metodo que determina la accion que se llevará a cabo a la hora de hacer click largo
         *   en un item de la lista
         */
        listaVideos.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                final CharSequence[] opciones;
                opciones            = new CharSequence[]{"Abrir con otro reproductor","Compartir","Eliminar"};
                objetoEnCuestion    =  nombres.get(position).toString();
                final String miRuta = ruta + "/"+objetoEnCuestion;
                final Uri uri       = Uri.parse(miRuta);

                builder = new AlertDialog.Builder(getActivity());
                builder.setTitle(nombres.get(position));
                builder.setItems(opciones, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int pos) {

                            switch (pos){
                                case 0:
                                    intent.reproductorExterno(uri);     //Para reproducir un video con otro reproducitor
                                    break;
                                case 1:
                                    intent.compartirArchivo(miRuta);    //Para compartir el video con otra aplicación
                                    break;
                                case 2:
                                    eliminarArchivo(miRuta);            // Para eliminar el video
                                    refrescarFragment();
                                break;
                            }
                    }
                });

                builder.show();

                return true ;

            }
        });//Fin istaVideos.setOnItemLongClickListener

    }//Fin onViewCreated

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }


   /*
    *   Metodo encargado de recoger los nombres y las miniaturas de los videos dentro de la carpeta de la app
    *   Usa la clase SearchResult para crear un ArrayList con las miniaturas y los nombres
    *   INPUT   NULL
    *   OUTPUT  ArrayList<SearchResults> results
    */
    private ArrayList<SearchResults> getSearchResults(){

        ArrayList<SearchResults> results = new ArrayList<SearchResults>();
        SearchResults sr1;

        if(isCarpetaCreada(nombreCarpeta)) {

            File carpeta    = new File(ruta);
            File[] archivos = carpeta.listFiles();

            if(archivos!=null) {

                for (int i = 0; i < archivos.length; i++) {

                    sr1 = new SearchResults();

                    if(!archivos[i].getName().equalsIgnoreCase("Thumbnails")){

                        String nombre   = archivos[i].getName();
                        String r        = ruta+"/"+nombre;

                        //Agregamos el nombre y la miniatura
                        nombres.add(nombre);
                        sr1.setName(nombre);
                        sr1.setBitmap( ThumbnailUtils.createVideoThumbnail(r,MediaStore.Images.Thumbnails.MINI_KIND));
                        results.add(sr1);

                    }
                }
            }
        }

        return results;

    }//Fin getSearchResults


    /*
     * Metodo que comprueba si la carpeta de la aplicación está creada en el sistema.
     * Si no está creada, la crea.
     * INPUT     String nombreCarpeta
     * OUTPUT    Boolean correcto;
     */
    public boolean isCarpetaCreada(String nombreCarpeta){
        File carpeta        = new File(Environment.getExternalStorageDirectory() + File.separator + nombreCarpeta);
        boolean correcto    = true;

        if (!carpeta.exists()) {
            correcto = carpeta.mkdirs();
        }
        if (!correcto) {

            toast.mensaje("Error al crear la carpeta");
        }

        return correcto;
    }//Fin isCarpetaCreada


    /*
     *  Metodo para eliminar un video
     *  INPUT   String ruta
     */
    public void eliminarArchivo(String ruta){

        File file       = new File(ruta);
        boolean borrado = file.delete();

        if(borrado){

            toast.mensaje("Video borrado correctamente");

        }else{

            toast.mensaje("Error al borrar el archivo");

        }

    }//Fin eliminarArchivo


    /*
     *  Metodo para refrescar el ActivityFragment después de hacer algún cambio
     */
    public void refrescarFragment(){

        getFragmentManager().beginTransaction().detach(this).attach(this).commit();

    }//Fin refrescarFragment

}//Fin class
