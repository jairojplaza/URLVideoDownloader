package com.example.theplazamen.uvdownloader;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class CustomAdapter extends BaseAdapter {

    private static ArrayList<SearchResults> searchArrayList;
    private LayoutInflater mInflater;


    public CustomAdapter(Context context, ArrayList<SearchResults> results) {

        searchArrayList = results;
        mInflater       = LayoutInflater.from(context);

    }

    public int getCount() {
        return searchArrayList.size();
    }

    public Object getItem(int position) {
        return searchArrayList.get(position);
    }

    public long getItemId(int position) {
        return position;
    }


    /*
     *  Metodo para obtener la vista de un video txtName + imageView
     *  Recibe una vista y la convierte para que contenga los 2 objetos
     *
     *  INPUT   int position
     *  INPUT   View view
     *  INPUT   ViewGroup parent
     *  OUTPUT  ViewHolder view
     */
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {

            convertView         = mInflater.inflate(R.layout.video_thumb, null);
            holder              = new ViewHolder();
            holder.txtName      = (TextView) convertView.findViewById(R.id.tvNombre);
            holder.imageView    = (ImageView) convertView.findViewById(R.id.ivVideo);

            convertView.setTag(holder);

        } else {

            holder = (ViewHolder) convertView.getTag();

        }

        holder.txtName.setText(searchArrayList.get(position).getName());
        holder.imageView.setImageBitmap(searchArrayList.get(position).getBitmap());

        return convertView;
    }


    /*
     *  Esquema de vista para mostrar nombre e imagen
     */
    static class ViewHolder {

        TextView txtName;
        ImageView imageView;

    }//Fin class

}//Fin class
