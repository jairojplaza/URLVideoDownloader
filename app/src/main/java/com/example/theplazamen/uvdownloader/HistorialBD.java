package com.example.theplazamen.uvdownloader;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class HistorialBD extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NOMBRE = "UVDownloader.db";

    public static final String TABLA_HISTORIAL = "Historial";
    public static final String COLUMNA_ID = "_id";
    public static final String COLUMNA_HISTORIAL = "historial";
    private Context context;
    private ToastMessage toast;
    private static final String SQL_CREAR = "create table "
            + TABLA_HISTORIAL + "(" + COLUMNA_ID
            + " integer primary key autoincrement, " + COLUMNA_HISTORIAL
            + " text not null);";


    //Constructor
    public HistorialBD(Context context) {


        super(context, DATABASE_NOMBRE, null, DATABASE_VERSION);
        this.context = context;

    }//Fin constructor


    //  Metodo onCreate encargado de crear la base de datos al intentar hacer insert
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREAR);
    }


    //Metodo onUpgrade para llevar la cuenta de las versiones
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){

    }


    /*
     *  Metodo insert, encargado de hacer un insert de una url
     *  INPUT String historial
     */
    public void insert(String historial){

        SQLiteDatabase db       = this.getWritableDatabase();
        ContentValues values    = new ContentValues();

        values.put(COLUMNA_HISTORIAL, historial);

        db.insert(TABLA_HISTORIAL, null,values);
        db.close();

    }//Fin insert


    /*
     *  Metodo select, para obtener una url a partir de su _id
     *  INPUT   int id
     *  OUTPUT  String url
     */
    public String select(int id){

        String s;

        SQLiteDatabase db   = this.getReadableDatabase();
        String[] projection = {COLUMNA_ID, COLUMNA_HISTORIAL};

        Cursor cursor =
                db.query(TABLA_HISTORIAL,
                        projection,
                        " _id = ?",
                        new String[] { String.valueOf(id) },
                        null,
                        null,
                        null,
                        null);


        if (cursor != null)
            cursor.moveToFirst();

        s = cursor.getString(1);

        db.close();

        if(s!=null && !s.isEmpty()){
            return s;
        }

        return null;
    }//Fin select


    /*
     *  Metodo selectAll, obtiene todas las url's, usado para listar
     *  INPUT   null
     *  OUTPUT  OBject {ids, urls}
     */
    public Object[] selectAll(){

        SQLiteDatabase db       = this.getReadableDatabase();
        String[] projection     = {COLUMNA_ID, COLUMNA_HISTORIAL};
        ArrayList<String> urls  = new ArrayList<>();
        ArrayList<String> ids   = new ArrayList<>();

        Cursor cursor =
                db.query(TABLA_HISTORIAL,
                        projection,
                        null,
                        null,
                        null,
                        null,
                        null);
        if (cursor != null) {

            cursor.moveToFirst();

        }else{

            toast.mensaje("Nulll weeeey");
            return null;

        }
        while(cursor.moveToNext()){

            String id   = cursor.getString(0);
            String url  = cursor.getString(1);

            ids.add(id);
            urls.add(url);
           // mensajeToast(id);
        }
        return new Object[]{ids,urls};
    }//Fin selectAll


    /*
     *  Metodo selectEmAll, obtiene todas las url's, usado para listar
     *  INPUT   null
     *  OUTPUT  ArrayList<String> urls
     */
   public ArrayList<String> selectEmAll(){

        SQLiteDatabase db       = this.getReadableDatabase();
        String[] projection     = {COLUMNA_ID, COLUMNA_HISTORIAL};
        ArrayList<String> urls  = new ArrayList<>();
        ArrayList<String> ids   = new ArrayList<>();

        Cursor cursor =
                db.query(TABLA_HISTORIAL,
                        projection,
                        null,
                        null,
                        null,
                        null,
                        null);

        if (cursor != null) {

            cursor.moveToFirst();

        }else{

            toast.mensaje("Nulll weeeey");
            return null;

        }
        while(cursor.moveToNext()){

            String url = cursor.getString(1);
            urls.add(url);

        }

        return urls;

    }//Fin selectEmAll


    /*
     *  Metodo para eliminar una url del historial a partir de su id
     *  INPUT   String id
     *  OUTPUT  null
     */
    public boolean delete(String id)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        return db.delete(COLUMNA_HISTORIAL, COLUMNA_ID + "=?", new String[]{id}) > 0;
    }

}//Fin class
