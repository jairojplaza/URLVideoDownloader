package com.example.theplazamen.uvdownloader;

import android.content.Context;
import android.widget.Toast;

public class ToastMessage {

    private Context context;

    //Constructor
    public ToastMessage(Context context){
        this.context = context;
    }

    /*
     *  Metodo para enviar mensaje
     *  INPUT   String mensaje
     *  OUTPUT  Toast
     */
    public void mensaje(String mensaje){

        Toast.makeText(context, mensaje, Toast.LENGTH_SHORT).show();
    }
}//Fin class
